Rails.application.routes.draw do
  root 'home#index'
  devise_for :admin_users
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  get 'users/dashboard'

  devise_for :users
  get 'home/index'
  get 'home/get_models_from_brand'
  get 'about_us' => 'home#about_us', :as => :about_us
  get 'contact_us' => 'home#contact', :as => :contact_us
  get 'faq' => 'home#faq', :as => :faq
  get 'privacy' => 'home#privacy_policy', :as => :privacy_policy
  get 'tos' => 'home#tos', :as => :site_tos
  post 'enquiry_mail' => 'home#enquiry_mail'

  resources :users do
    collection do
      get 'myads'
      get 'pending_approval_ads'
      get 'saved_ads'
      put 'remove_saved_ad'
    end
  end

  resources :ads do
    collection do
      get 'get_model'
      get 'model_to_variants'
      get 'success'
      post 'email_to_seller'
      post 'save_ad'
      get :compare
    end
    member do
      get 'tag_spam'
    end
  end

  resources :shortlists do
    member do
      post 'add_ad'
    end
  end

  get 'title_list' => 'ads#title_list'
  get 'location_list' => 'ads#location_list'
  get 'category_list' => 'ads#category_list'

  namespace :api do
    namespace :v1 do
      devise_for :users
      resources :ads, only: [:index, :create] do
        collection do
          post 'show_ad', to: 'ads#show'
        end
        delete :destroy
      end
      resources :users, only: [:dashboard] do
        collection do
          post :dashboard
        end
      end
      get 'brands', to: 'templates#get_brand_names'
      get 'emirates', to: 'templates#get_emirate_names'
      get 'brands/:id', to: 'templates#get_model_names'
      post 'profile_data', to: 'templates#get_profile_data'
      post 'search', to: 'ads#search'
    end
  end

  resources :brands do
    collection do
      get 'get_brands_from_category'
    end
  end

  resources :vehicle_models do
    collection do
      get 'get_vehicle_models_from_brand'
    end
  end

end
