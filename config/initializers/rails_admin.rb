require Rails.root.join('lib', 'rails_admin_approve_ad.rb')
RailsAdmin::Config::Actions.register(RailsAdmin::Config::Actions::ApproveAd)

RailsAdmin.config do |config|
  ### Popular gems integration

  ## == Devise ==
  # config.authenticate_with do
  #   warden.authenticate! scope: :user
  # end
  # config.current_user_method(&:current_user)

  ## == Cancan ==
  config.authorize_with :cancan

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app
    approve_ad do
      visible do
        bindings[:abstract_model].model.to_s == 'Ad'
      end
    end

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  config.model 'Ad' do
    list do
      field :title
      field :description
      field :price
      field :is_approved
      field :user
      field :photo do
        pretty_value do
          bindings[:view].tag(:img, { :src => bindings[:object].first_thumb_image })
        end
      end
    end

    show do
      field :title
      field :description
      field :price
      field :is_approved
      field :negotiable
      field :user
      field :vehicle_detail do
        pretty_value do
          bindings[:view].render(
            partial: "admin/vehicle_detail",
            locals: {vehicle_detail:bindings[:object].vehicle_detail}
          )
        end
      end
      field :seller_information do
        pretty_value do
          bindings[:view].render(
            partial: "admin/seller_information",
            locals: {seller_information:bindings[:object].seller_information}
          )
        end
      end
      field :photos do
        pretty_value do
          bindings[:view].render(
            partial: "admin/photos",
            locals: {ad:bindings[:object]}
          )
        end
      end
    end
    weight 1
  end

  config.model 'VehicleDetail' do
    parent Ad
    list do
      field  :fuel
      field  :color
      field  :mileage
      field  :manufacturing_date
      field  :insurance_date
      field  :owner_count
      field  :ad
      field  :brand
      field  :vehicle_model
      field  :variant
    end
    weight 1
  end

  config.model 'SellerInformation' do
    parent Ad
    list do
      field  :name
      field  :email
      field  :phone_number
      field  :ad
      field  :hide_name
      field  :hide_email
      field  :hide_phone
    end
    weight 2
  end

  config.model 'Photo' do
    parent Ad
    weight 3
    exclude_fields :created_at,:updated_at
  end

  config.model 'Brand' do
    list do
      field :name
      field :created_at
    end
    weight 2
  end

  config.model 'VehicleModel' do
    list do
      field :name
      field :brand
      field :created_at
    end
    weight 3
  end

  config.model 'Variant' do
    list do
      field :name
      field :vehicle_model
      field :created_at
    end
    weight 4
  end

  config.model 'User' do
    edit do
      exclude_fields :reset_password_sent_at,:remember_created_at,:sign_in_count,:current_sign_in_at,:last_sign_in_at,:current_sign_in_ip,:last_sign_in_ip,:confirmation_token,:confirmed_at,:confirmation_sent_at,:unconfirmed_email,:authentication_token,:updated_at
      field :ads do
        read_only true
      end
    end

    list do
      field :name
      field :email
      field :created_at
      field :updated_at
    end
    weight 5
  end

  config.model 'Country' do
    list do
      field :name
      field :emirates
      field :created_at
    end
    weight 6
  end

  config.model 'Emirate' do
    list do
      field :name
      field :country
      field :created_at
    end
    weight 7
  end

  config.model 'AdminUser' do
    edit do
      exclude_fields :reset_password_sent_at,:remember_created_at,:sign_in_count,:current_sign_in_at,:last_sign_in_at,:current_sign_in_ip,:last_sign_in_ip,:confirmation_token,:confirmed_at,:confirmation_sent_at,:unconfirmed_email
    end

    show do
      field :name
      field :email
      field :created_at
      field :updated_at
    end
    weight 8
  end

  config.model 'Spam' do
    list do
      field :ad_id
      field :user_id
    end
  end

  config.excluded_models = [Shortlist,Plan]

  config.authenticate_with do
    warden.authenticate! scope: :admin_user
  end
  config.current_user_method(&:current_admin_user)
end

