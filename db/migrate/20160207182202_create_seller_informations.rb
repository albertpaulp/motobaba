class CreateSellerInformations < ActiveRecord::Migration
  def change
    create_table :seller_informations do |t|
    	t.string :name
    	t.string :email
    	t.string :phone_number
    	t.boolean :show_name, default: true
    	t.boolean :show_email, default: true
    	t.boolean :show_phone, default: true
      t.timestamps null: false
    end
  end
end
