class AddEngineSpecsToVehicleDetails < ActiveRecord::Migration
  def change
    add_column :vehicle_details, :cylinder, :string
    add_column :vehicle_details, :capacity, :string
  end
end
