class CreateAds < ActiveRecord::Migration
  def change
    create_table :ads do |t|
    	t.string :title
    	t.string :type
    	t.string :category
    	t.string :country
    	t.string :city
    	t.string :location
    	t.string :phone
    	t.boolean :show_phone
    	t.string :status
    	t.belongs_to :user, index: true
        t.belongs_to :plan, index: true
      t.timestamps null: false
    end
  end
end
