class AddPriceToAd < ActiveRecord::Migration
  def change
  	add_column :ads, :price, :decimal, :precision => 8, :scale => 2
  	remove_column :vehicle_details, :price
  end
end
