class AddDetailToAd < ActiveRecord::Migration
  def change
  	add_column :ads, :description, :text
  	remove_column :vehicle_details, :description
  end
end
