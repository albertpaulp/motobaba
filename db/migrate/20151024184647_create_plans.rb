class CreatePlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
    	t.string :name
    	t.decimal :price, :precision => 8, :scale => 2
    	t.integer :validity
      t.timestamps null: false
    end
  end
end
