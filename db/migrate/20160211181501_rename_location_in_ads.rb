class RenameLocationInAds < ActiveRecord::Migration
  def change
    rename_column :ads, :country, :country_id
    rename_column :ads, :city, :emirate_id
  end
end
