class AddIsApprovedInToAd < ActiveRecord::Migration
  def change
    add_column :ads, :is_approved, :boolean, :default => false
  end
end
