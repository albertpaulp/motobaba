class CreateVariants < ActiveRecord::Migration
  def change
    create_table :variants do |t|
    	t.string :name
    	t.belongs_to :vehicle_model, index: true
      t.timestamps null: false
    end
  end
end
