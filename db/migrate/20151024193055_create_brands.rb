class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
    	t.string :name
    	t.boolean :is_popular
    	t.string :category
      t.timestamps null: false
    end
  end
end
