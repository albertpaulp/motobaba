class CreateSpams < ActiveRecord::Migration
  def change
    create_table :spams do |t|
      t.integer :user_id
      t.string :ad_id
      t.string :status

      t.timestamps null: false
    end
  end
end
