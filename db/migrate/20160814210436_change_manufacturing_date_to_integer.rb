class ChangeManufacturingDateToInteger < ActiveRecord::Migration
  def change
    remove_column :vehicle_details, :manufacturing_date
    add_column :vehicle_details, :manufacturing_date, :integer
  end
end
