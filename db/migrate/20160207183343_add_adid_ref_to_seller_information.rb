class AddAdidRefToSellerInformation < ActiveRecord::Migration
  def change
    add_reference :seller_informations, :ad, index: true
  end
end
