class RenameZipToEmirate < ActiveRecord::Migration
  def change
    remove_column :users, :zipcode
    add_column :users, :emirate_id, :integer
  end
end
