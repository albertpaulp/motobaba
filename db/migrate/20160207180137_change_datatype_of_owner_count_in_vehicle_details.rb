class ChangeDatatypeOfOwnerCountInVehicleDetails < ActiveRecord::Migration
  def change
  	change_column :vehicle_details, :owner_count, :string
  end
end
