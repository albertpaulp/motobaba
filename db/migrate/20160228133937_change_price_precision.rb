class ChangePricePrecision < ActiveRecord::Migration
  def change
  	change_column :ads, :price, :decimal, :precision => 10, :scale => 2
  end
end
