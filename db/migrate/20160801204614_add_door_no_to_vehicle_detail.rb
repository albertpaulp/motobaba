class AddDoorNoToVehicleDetail < ActiveRecord::Migration
  def change
    add_column :vehicle_details, :door_no, :integer
  end
end
