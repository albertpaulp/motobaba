class MoveTransmissionFromAds < ActiveRecord::Migration
  def change
    remove_column :ads, :transmission
    add_column :vehicle_details, :transmission, :string
  end
end
