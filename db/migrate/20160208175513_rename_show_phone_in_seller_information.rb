class RenameShowPhoneInSellerInformation < ActiveRecord::Migration
  def change
    change_table :seller_informations do |t|
      t.rename :show_phone, :hide_phone
      t.rename :show_name, :hide_name
      t.rename :show_email, :hide_email
    end
  end
end
