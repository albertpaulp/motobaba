class RenameCountryToLocation < ActiveRecord::Migration
  def change
  	rename_column :users, :country, :location
  end
end
