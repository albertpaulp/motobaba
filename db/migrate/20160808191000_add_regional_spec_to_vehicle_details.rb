class AddRegionalSpecToVehicleDetails < ActiveRecord::Migration
  def change
    add_column :vehicle_details, :regional_spec, :string
    add_column :vehicle_details, :horse_power, :string
  end
end
