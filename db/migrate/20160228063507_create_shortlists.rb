class CreateShortlists < ActiveRecord::Migration
  def change
    create_table :shortlists do |t|
      t.references :user, index: true, foreign_key: true
      t.references :ad, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
