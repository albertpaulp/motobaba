class OptimizeAdsTable < ActiveRecord::Migration
  def change
    remove_column :ads, :category
    add_column :ads, :category_id, :integer, index: true
    remove_column :ads, :country_id
    add_column :ads, :country_id, :integer, index: true
    remove_column :ads, :emirate_id
    add_column :ads, :emirate_id, :integer, index: true
    add_index(:ads, :price)
    remove_column :ads, :is_approved
    add_column :ads, :is_approved, :boolean, index: true
  end
end
