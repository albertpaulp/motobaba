class ChangeManufacturingDateTypeInVehicleDetail < ActiveRecord::Migration
  def change
  	change_column :vehicle_details, :manufacturing_date, :string
  end
end
