class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.text :address
      t.string :city
      t.integer :zipcode
      t.integer :phone
      t.boolean :is_active, default:true
      t.timestamps null: false
    end
  end
end
