class AddFlagsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :newsletter, :boolean
    add_column :users, :mail_com, :boolean
  end
end
