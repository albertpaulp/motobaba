class RemoveColumnFromAds < ActiveRecord::Migration
  def change
    remove_column :ads, :phone
    remove_column :ads, :show_phone
  end
end
