class ChangeTypeofCylinderFromVehicleDetails < ActiveRecord::Migration
  def change
     change_column :vehicle_details, :cylinder, :string
  end
end
