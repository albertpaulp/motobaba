class CreateVehicleDetails < ActiveRecord::Migration
  def change
    create_table :vehicle_details do |t|
    	t.date :manufacturing_date
    	t.integer :mileage
    	t.string :fuel
    	t.decimal :price, :precision => 8, :scale => 2
    	t.string :color
    	t.date :insurance_date
    	t.text :description
    	t.integer :owner_count
    	t.belongs_to :ad, index: true
      t.belongs_to :brand, index: true
      t.belongs_to :vehicle_model, index: true
      t.belongs_to :variant, index: true
      t.timestamps null: false
    end
  end
end
