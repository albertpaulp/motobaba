class RemoveUnneccessaryColumnsFromSellerInformations < ActiveRecord::Migration
  def change
    remove_column :seller_informations, :hide_name
    remove_column :seller_informations, :hide_email
    remove_column :seller_informations, :hide_phone
  end
end
