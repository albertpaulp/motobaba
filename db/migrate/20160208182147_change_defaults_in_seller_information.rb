class ChangeDefaultsInSellerInformation < ActiveRecord::Migration
  def change
      change_column_default :seller_informations, :hide_name, false
      change_column_default :seller_informations, :hide_email, false
      change_column_default :seller_informations, :hide_phone, false
  end
end
