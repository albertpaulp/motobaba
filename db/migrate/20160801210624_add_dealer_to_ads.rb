class AddDealerToAds < ActiveRecord::Migration
  def change
    add_column :ads, :dealer, :boolean, default: false
  end
end
