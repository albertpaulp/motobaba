class CreateEmirates < ActiveRecord::Migration
  def change
    create_table :emirates do |t|
      t.string :name, index: true
      t.integer :country_id, index: true
      t.timestamps null: false
    end
  end
end
