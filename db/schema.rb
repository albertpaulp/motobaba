# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170305124510) do

  create_table "admin_users", force: :cascade do |t|
    t.string   "name",                   limit: 255
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "ads", force: :cascade do |t|
    t.string   "title",       limit: 255
    t.string   "ad_type",     limit: 255
    t.string   "location",    limit: 255
    t.string   "status",      limit: 255
    t.integer  "user_id",     limit: 4
    t.integer  "plan_id",     limit: 4
    t.datetime "created_at",                                                         null: false
    t.datetime "updated_at",                                                         null: false
    t.boolean  "negotiable",                                         default: true
    t.text     "description", limit: 65535
    t.decimal  "price",                     precision: 10, scale: 2
    t.boolean  "dealer",                                             default: false
    t.string   "name",        limit: 255
    t.integer  "category_id", limit: 4
    t.integer  "country_id",  limit: 4
    t.integer  "emirate_id",  limit: 4
    t.boolean  "is_approved"
    t.string   "state",       limit: 255
  end

  add_index "ads", ["plan_id"], name: "index_ads_on_plan_id", using: :btree
  add_index "ads", ["price"], name: "index_ads_on_price", using: :btree
  add_index "ads", ["user_id"], name: "index_ads_on_user_id", using: :btree

  create_table "brands", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.boolean  "is_popular"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "category_id", limit: 4
  end

  create_table "categories", force: :cascade do |t|
    t.string "name", limit: 255
  end

  create_table "countries", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "countries", ["name"], name: "index_countries_on_name", using: :btree

  create_table "emirates", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "country_id", limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "emirates", ["country_id"], name: "index_emirates_on_country_id", using: :btree
  add_index "emirates", ["name"], name: "index_emirates_on_name", using: :btree

  create_table "photos", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.integer  "imageable_id",       limit: 4
    t.string   "imageable_type",     limit: 255
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
  end

  add_index "photos", ["imageable_type", "imageable_id"], name: "index_photos_on_imageable_type_and_imageable_id", using: :btree

  create_table "plans", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.decimal  "price",                  precision: 8, scale: 2
    t.integer  "validity",   limit: 4
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
  end

  create_table "seller_informations", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.string   "email",        limit: 255
    t.string   "phone_number", limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "ad_id",        limit: 4
  end

  add_index "seller_informations", ["ad_id"], name: "index_seller_informations_on_ad_id", using: :btree

  create_table "shortlists", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "ad_id",      limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "shortlists", ["ad_id"], name: "index_shortlists_on_ad_id", using: :btree
  add_index "shortlists", ["user_id"], name: "index_shortlists_on_user_id", using: :btree

  create_table "spams", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.string   "ad_id",      limit: 255
    t.string   "status",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.string   "city",                   limit: 255
    t.string   "phone",                  limit: 255
    t.boolean  "is_active",                          default: true
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.string   "email",                  limit: 255, default: "",   null: false
    t.string   "encrypted_password",     limit: 255, default: "",   null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",      limit: 255
    t.string   "gender",                 limit: 255
    t.string   "location",               limit: 255
    t.boolean  "newsletter"
    t.boolean  "mail_com"
    t.string   "authentication_token",   limit: 255
    t.integer  "emirate_id",             limit: 4
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "variants", force: :cascade do |t|
    t.string   "name",             limit: 255
    t.integer  "vehicle_model_id", limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "variants", ["vehicle_model_id"], name: "index_variants_on_vehicle_model_id", using: :btree

  create_table "vehicle_details", force: :cascade do |t|
    t.integer  "mileage",             limit: 4
    t.string   "fuel",                limit: 255
    t.string   "color",               limit: 255
    t.date     "insurance_date"
    t.string   "owner_count",         limit: 255
    t.integer  "ad_id",               limit: 4
    t.integer  "brand_id",            limit: 4
    t.integer  "vehicle_model_id",    limit: 4
    t.integer  "variant_id",          limit: 4
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "door_no",             limit: 4
    t.string   "cylinder",            limit: 255
    t.string   "capacity",            limit: 255
    t.boolean  "radio"
    t.boolean  "alarm_antitheft"
    t.boolean  "aux_audio"
    t.boolean  "bluetooth"
    t.boolean  "body_kit"
    t.boolean  "brush_guard"
    t.boolean  "cd_player"
    t.boolean  "dvd_player"
    t.boolean  "climate_control"
    t.boolean  "cooled_seats"
    t.boolean  "heat"
    t.boolean  "heated_seats"
    t.boolean  "keyless_entry"
    t.boolean  "keyless_start"
    t.boolean  "leather_seat"
    t.boolean  "moonroofs"
    t.boolean  "navigation"
    t.boolean  "offroad_kit"
    t.boolean  "offroad_tyre"
    t.boolean  "parking_sensor"
    t.boolean  "power_mirror"
    t.boolean  "power_seats"
    t.boolean  "power_sunroof"
    t.boolean  "power_windows"
    t.boolean  "premium_lights"
    t.boolean  "premium_paint"
    t.boolean  "premium_soundsystem"
    t.boolean  "premium_wheels"
    t.boolean  "racing_seats"
    t.boolean  "roof_rack"
    t.boolean  "satellite_radio"
    t.boolean  "spoiler"
    t.boolean  "sunroof"
    t.boolean  "fourwheel_drive"
    t.boolean  "winch"
    t.boolean  "allwheel_drive"
    t.boolean  "abs"
    t.boolean  "cruise_control"
    t.boolean  "dual_exhaust"
    t.boolean  "front_airbags"
    t.boolean  "frontwheel_drive"
    t.boolean  "n2o"
    t.boolean  "power_steering"
    t.boolean  "rearwheel_drive"
    t.boolean  "side_airbags"
    t.boolean  "tiptronic_gears"
    t.string   "regional_spec",       limit: 255
    t.string   "horse_power",         limit: 255
    t.string   "transmission",        limit: 255
    t.integer  "manufacturing_date",  limit: 4
  end

  add_index "vehicle_details", ["ad_id"], name: "index_vehicle_details_on_ad_id", using: :btree
  add_index "vehicle_details", ["brand_id"], name: "index_vehicle_details_on_brand_id", using: :btree
  add_index "vehicle_details", ["variant_id"], name: "index_vehicle_details_on_variant_id", using: :btree
  add_index "vehicle_details", ["vehicle_model_id"], name: "index_vehicle_details_on_vehicle_model_id", using: :btree

  create_table "vehicle_models", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "brand_id",   limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "vehicle_models", ["brand_id"], name: "index_vehicle_models_on_brand_id", using: :btree

  add_foreign_key "shortlists", "ads"
  add_foreign_key "shortlists", "users"
end
