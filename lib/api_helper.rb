module ApiHelper
  def validate_auth_token
    get_current_user
    render :status => 401, :json => {errors: ['invalid_token']} if @user.nil?
  end

  def self.included(base)
    base.class_eval do
      rescue_from Apipie::ParamMissing do |exception|
        render status: 400, json: {
          param: exception.param,
          error: exception.to_s
        }
      end
      rescue_from Apipie::ParamInvalid do |exception|
        render status: 400, json: {
          param: exception.param,
          error: exception.to_s
        }
      end
    end
  end

  def get_auth_token
    if params[:auth_token].blank?
      logger.debug "Auth Token: #{request.headers['AUTH-TOKEN']}"
      params[:auth_token] = request.headers['AUTH-TOKEN']
    end
  end

  def get_current_user
    @user = User.find_by_authentication_token(params[:auth_token]) unless params[:auth_token].blank?
  end
end
