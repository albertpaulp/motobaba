class VehicleDetail < ActiveRecord::Base
  FIELDS_RENDERED = [
      :vehicle_model_id, :variant_id, :manufacturing_date, :mileage, :fuel, :color,
      :insurance_date, :owner_count, :_destroy, :door_no, :cylinder, :capacity,
      :regional_spec, :horse_power, :cruise_control, :navigation, :fourwheel_drive,
      :allwheel_drive, :body_kit, :climate_control, :keyless_entry, :keyless_start,
      :radio, :aux_audio, :offroad_kit, :offroad_tyre, :parking_sensor, :moonroofs,
      :cd_player, :dvd_player, :power_mirror, :power_seats, :power_sunroof,
      :power_windows, :premium_lights, :premium_paint, :premium_soundsystem,
      :premium_wheels, :racing_seats, :roof_rack, :satellite_radio, :spoiler, :sunroof,
      :winch, :dual_exhaust, :power_steering, :n2o, :tiptronic_gears, :brush_guard,
      :heated_seats, :heat, :cooled_seats, :transmission, :abs, :front_airbags,
      :side_airbags, :bluetooth, :alarm_antitheft]
	belongs_to :ad
	belongs_to :brand
	belongs_to :vehicle_model
	belongs_to :variant

	def self.fuel_types
		{
			Petrol: 'Petrol',
			Diesal: 'Diesal',
			Electric: 'Electric',
			Hybrid: 'Hybrid',
			Lpg: 'LPG'
		}
	end

	def self.color
		{
			beige: 'Beige',
			black: 'Black',
			blue: 'Blue',
			golden: 'Golden',
			green: 'Green',
			grey: 'Grey',
			orange: 'Orange',
			purple: 'Purple',
			red: 'Red',
			silver: 'Silver',
			white: 'White',
			yellow: 'Yellow',
			other: 'Other'
		}
	end

end
