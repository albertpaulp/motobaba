class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable
         
  validates_presence_of :first_name

  before_create :generate_authentication_token

  has_many :shortlists,dependent: :destroy
  has_many :ads, dependent: :destroy

  def name
  	"#{first_name} #{last_name}"
  end

  def last_login
  	last_sign_in_at.strftime("%d-%m-%Y %H:%M:%S [%z %Z]")
  end

  def ensure_authentication_token!
    if authentication_token.blank?
      self.authentication_token = generate_authentication_token
    end
  end

  private

  def generate_authentication_token
    self.authentication_token = SecureRandom.base64(64)
  end
end
