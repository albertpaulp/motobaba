class Ad < ActiveRecord::Base

  searchable do
    float :price
    integer :emirate_id
    boolean :dealer
    boolean :is_approved
    time :created_at
    integer :brand_id do
      vehicle_detail.try(:brand_id)
    end
    integer :vehicle_model_id do
      vehicle_detail.try(:vehicle_model_id)
    end
    integer :mileage do
      vehicle_detail.try(:mileage)
    end
    integer :door_no do
      vehicle_detail.try(:door_no)
    end
    integer :manufacturing_date do
      vehicle_detail.try(:manufacturing_date)
    end
  end

  METHODS_RENDERED = [:emirate_name, :image_path]
  FIELDS_RENDERED = [:id, :name, :title, :location, :description, :price,
                        :created_at, :plan_id, :negotiable, :dealer, :is_approved]
  belongs_to :user
  belongs_to :plan
  belongs_to :country
  belongs_to :emirate
  has_one :seller_information ,dependent: :destroy
  has_one :vehicle_detail, dependent: :destroy
  has_many :photos, as: :imageable ,dependent: :destroy
  # has_many :featured_ads,dependent: :destroy
  paginates_per 10

  accepts_nested_attributes_for :vehicle_detail, :allow_destroy => true
  accepts_nested_attributes_for :photos, :allow_destroy => true
  accepts_nested_attributes_for :seller_information, :allow_destroy => true


  validates :price, numericality: true

  scope :approved, -> { where(is_approved: true) }
  scope :unapproved, -> { where(is_approved: false) }

  def self.dealer_types
    {
      Private: 'private',
      Dealer: 'dealer'
    }
  end

  def as_json(options={})
    super(:methods => Ad::METHODS_RENDERED,
          :include => [
            { :vehicle_detail =>
                {  :only => VehicleDetail::FIELDS_RENDERED }
            },
            { :seller_information =>
                {  :only => SellerInformation::FIELDS_RENDERED }
            }
          ],
          :only => Ad::FIELDS_RENDERED)
  end

  def first_thumb_image
    photos.first.image.url(:thumb) if photos.present?
  end

  def brand_name
    vehicle_detail.try(:brand).try(:name)
  end

  def vehicle_model_name
    vehicle_detail.try(:vehicle_model).try(:name)
  end

  def variant_name
    vehicle_detail.try(:varient).try(:name)
  end

  def seller_name
    seller_information.try(:name).try(:titlecase)
  end

  def image_path
    {
      :original => photos.map{|p| p.image.url},
      :medium => photos.map{|p| p.image.url(:medium)},
      :thumb => photos.map{|p| p.image.url(:thumb)}
    }
  end

  def emirate_name
    emirate.try(:name)
  end

  def created_time
    DateTime.current
  end
end
