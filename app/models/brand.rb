class Brand < ActiveRecord::Base
	has_many :vehicle_models

	def category_enum
		{
			:car => "Car",
			:bike => "Bike",
			:commercial_vehicle => "Commercial Vehicle"
		}
	end
end
