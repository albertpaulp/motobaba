class Photo < ActiveRecord::Base
  METHODS_RENDERED = [:thumb_image_path]
	belongs_to :imageable, polymorphic: true

	has_attached_file :image,
    styles: {
      thumb: '125x125!',
      medium: '400x400',
    },
    default_url: "/images/:style/missing.png"

  validates_attachment :image,
    presence: true,
    content_type: { content_type: "image/jpeg" },
    size: { in: 0..5.megabytes }

  def thumb_image_path
    {
     :thumb => image.url(:thumb),
     :medium => image.url(:medium)
    }
  end
end
