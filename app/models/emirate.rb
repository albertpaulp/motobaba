class Emirate < ActiveRecord::Base
  belongs_to :country
  validates :name, presence: true

  def title
  	name
  end
end
