class SellerInformation < ActiveRecord::Base
  FIELDS_RENDERED = [:name, :email, :phone_number]
	belongs_to :ad
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }
end
