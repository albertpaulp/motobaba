class VehicleModel < ActiveRecord::Base
	belongs_to :brand
	has_many :variants
end
