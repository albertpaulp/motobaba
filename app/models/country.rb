class Country < ActiveRecord::Base
  has_many :emirates
  validates :name, presence: true
end
