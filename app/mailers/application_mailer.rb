class ApplicationMailer < ActionMailer::Base
  default from: "no-reply@motobaba.com"
  layout 'mailer'
end
