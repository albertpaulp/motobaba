class UserMailer < ApplicationMailer

	def email_to_seller ad_id,to_mail,seller_name, sender_name, sender_email,recipient_phone_number,message_text 
		@ad_id = ad_id
		@seller_name = seller_name
		@sender_name = sender_name 
		@sender_email = sender_email
		@recipient_phone_number = recipient_phone_number
		@message_text = message_text
		mail(to: to_mail, subject: "Enquiry from #{sender_name}")
	end

	def enquiry_mail first_name,last_name,company_name,email,message
		@first_name = first_name
		@last_name = last_name
		@company_name = company_name
		@email = email
		@message = message
		mail(to: 'eshaiju@gmail.com,admin@motobaba.com', subject: "Enquiry Mail From #{first_name}")
	end
end

