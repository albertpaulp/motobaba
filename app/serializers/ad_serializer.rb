class AdSerializer < ActiveModel::Serializer
  attributes :id, :title, :ad_type, :location, :status, :user_id, :plan_id, :created_at, :updated_at, :negotiable, :description,
             :price, :dealer, :name, :category_id, :country_id, :emirate_id, :is_approved
end
