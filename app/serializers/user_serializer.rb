class UserSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :email, :phone, :city, :is_active, :gender, :emirate_id, :last_sign_in_at
end
