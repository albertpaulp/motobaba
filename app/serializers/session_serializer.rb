class SessionSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :email, :token

  def token
    object.authentication_token
  end
end
