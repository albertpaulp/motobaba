class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery
  before_filter :update_sanitized_params, if: :devise_controller?

protected

  def update_sanitized_params
    added_attrs = [:first_name, :last_name, :address, :zipcode, :city, :country, :phone, :gender]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end
end
