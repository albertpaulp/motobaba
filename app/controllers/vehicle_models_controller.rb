class VehicleModelsController < ApplicationController
  def get_vehicle_models_from_brand
    models = VehicleModel.where(brand_id: params[:brand_id])
    render json: models.as_json(only: [:id, :name])
  end
end
