class BrandsController < ApplicationController
  def get_brands_from_category
    brands = Brand.where(category_id: params[:category_id])
    render json: brands.as_json(only: [:id, :name])
  end
end
