class ShortlistsController < ApplicationController

  def add_ad
    if current_user.nil?
      @result = 'nologin'
    elsif Shortlist.exists?(user_id: current_user.id, ad_id: params[:ad_id])
      @result = 'exist'
    else
      shortlist = Shortlist.new(user_id: current_user.id, ad_id: params[:ad_id])
      @result = shortlist.save
    end
    respond_to do |format|
      format.js
    end
  end
end
