class AdsController < ApplicationController
  before_action :authenticate_ad_posting , only: [:create,:new]

  def index
    @emirates = Emirate.all
    params[:brand].present? ? @models = VehicleModel.where(brand_id: params[:brand]) : @models = VehicleModel.all
    ads = Ad.search do
      order_by(:created_at, :desc)
      if params[:brand] && params[:brand] != ''
        with(:brand_id, params[:brand])
      end
      if params[:model] && params[:model] != ''
        with(:vehicle_model_id, params[:model])
      end
      if params[:emirate] && params[:emirate] != ''
        with(:emirate_id, params[:emirate])
      end
      if params[:min_price] && params[:min_price] != '' && params[:max_price] && params[:max_price] != ''
        with(:price).between(params[:min_price]..params[:max_price])
      end
      with(:is_approved, true)
      order_by :created_at, :desc
      paginate page: params[:page], :per_page => 10
    end
    @ads = ads.results
  end

  def show
  	@ad = Ad.find_by_id(params[:id])
  end

	def new
		@ad = Ad.new
		@ad.build_vehicle_detail
		@ad.build_seller_information
	end

	def create
		ad = Ad.new(ads_params)
		ad.ad_type = "sell"
    ad.seller_information.email = "anonymousabc.com" if ad.seller_information.email == "Anonymous"
    ad.name = Brand.where(
      id: params[:ad][:vehicle_detail_attributes][:brand_id]).first.try(:name) + " " +
      VehicleModel.where(id: params[:ad][:vehicle_detail_attributes][:vehicle_model_id])
      .first.try(:name)
		ad.user_id = current_user.id
    ad.is_approved = false
    ad.state = 'initiated'
		if ad.save
			if params[:photos]
	        params[:photos].each { |image|
	          ad.photos.create(image: image)
	        }
	      end
	  end
    redirect_to :success_ads
	end

  def success
  end

  def destroy
  	ad = current_user.ads.where(id: params[:id]).first
  	ad.destroy
  	render :json => { }
  end

  def title_list
    list = Ad.approved.collect {|ad| {:id => ad.title,:name => "#{ad.title}" }}.compact.uniq
    render :json => list
  end

  def category_list
    list = Ad.categories.collect {|category| {:id => category[0],:name => category[1] }}
    render :json => list
  end

  def location_list
    list = Ad.approved.collect {|ad| {:id => ad.location,:name => "#{ad.location}" }}.compact.uniq
    render :json => list
  end

  def email_to_seller
    UserMailer.email_to_seller(params[:ad_id],params[:to_mail],params[:seller_name],params[:sender_name],params[:sender_email],params[:recipient_phone_number],params[:message_text]).deliver_later
    respond_to do |format|
      format.js { render layout: false, content_type: 'text/javascript' }
    end
  end

	def get_model
		models = Brand.find(params[:id]).vehicle_models.pluck(:id,:name)
		initial_variant = Variant.where(vehicle_model_id: models.first[0]).pluck(:id, :name)
    respond_to do |format|
    	format.json {render json: {models: models, initial_variant: initial_variant}}
    end
	end

  def authenticate_ad_posting
    flash[:notice] = "Please login to post free advertisement." unless signed_in?
    authenticate_user!
  end

  def save_ad
    short_lists = Shortlist.where(ad_id: params[:id],user_id:params[:user_id])
    if short_lists.present?
      @message ='Already short listed'
      respond_to do |format|
        format.js { render layout: false, content_type: 'text/javascript' }
      end
    else
      Shortlist.create(ad_id: params[:id],user_id:params[:user_id])
      @message='Successfully short listed'
      respond_to do |format|
        format.js { render layout: false, content_type: 'text/javascript' }
      end
    end
  end

  def tag_spam
    if current_user.nil?
      @result = 'nologin'
    else
      Spam.create(user_id: current_user.id, ad_id: params[:ad_id])
    end
    respond_to do |format|
      format.js
    end
  end

  def compare
    ads_cookies = JSON.parse(cookies[:ads_to_compare])
    if ads_cookies.count < 4 && ads_cookies.count > 1
      ad_ids_to_compare = ads_cookies.map{|a| a['id']}
    elsif ads_cookies.count < 2
      flash[:alert] = 'Select minimum of 2 ads or maximum of 3 ads for comparison !'
      redirect_to ads_path
    end
    @ads = Ad.where(id: ad_ids_to_compare)
    @ad1 = @ads.first
    @ad2 = @ads.last(2).first
    @ad3 = @ads.last if @ads.count > 2
  end

private

	def ads_params
    params.require(:ad).permit(:type, :category_id, :country_id, :emirate_id, :location,
      :phone, :status, :description, :price, :negotiable,
      :name, :title, :dealer,
    vehicle_detail_attributes: [:id, :brand_id,
      :vehicle_model_id, :variant_id, :manufacturing_date, :mileage, :fuel, :color,
      :insurance_date, :owner_count, :_destroy, :door_no, :cylinder, :capacity,
      :regional_spec, :horse_power, :cruise_control, :navigation, :fourwheel_drive,
      :allwheel_drive, :body_kit, :climate_control, :keyless_entry, :keyless_start,
      :radio, :aux_audio, :offroad_kit, :offroad_tyre, :parking_sensor, :moonroofs,
      :cd_player, :dvd_player, :power_mirror, :power_seats, :power_sunroof,
      :power_windows, :premium_lights, :premium_paint, :premium_soundsystem,
      :premium_wheels, :racing_seats, :roof_rack, :satellite_radio, :spoiler, :sunroof,
      :winch, :dual_exhaust, :power_steering, :n2o, :tiptronic_gears, :brush_guard,
      :heated_seats, :heat, :cooled_seats, :transmission, :abs, :front_airbags,
      :side_airbags, :bluetooth, :alarm_antitheft],
     seller_information_attributes: [:name, :email, :phone_number])
  end

end
