class Api::V1::BaseController < ApplicationController
  protect_from_forgery with: :null_session
  before_action :authenticate_user!
  before_action :destroy_session

  def destroy_session
    request.session_options[:skip] = true
  end

  def authenticate_user!
    user = User.find_by(authentication_token: params[:auth_token])
    if user
      sign_in(user)
    else
      render(
         json: { message: 'Incorrect authentication token !'},
        status: 403
      )
    end
  end

  def current_user
    User.find_by(authentication_token: params[:auth_token])
  end

  private
  
  def secure_compare(a, b)
    return false unless a.bytesize == b.bytesize
    l = a.unpack "C#{a.bytesize}"
    res = 0
    b.each_byte { |byte| res |= byte ^ l.shift }
    res == 0
  end
end
