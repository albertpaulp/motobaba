class Api::V1::UsersController < Api::V1::BaseController
  def dashboard
    my_ads = current_user.ads
    pending_ads = current_user.ads.unapproved
    short_list_ads = Shortlist.where(user_id:current_user.id)
    if my_ads.present?
      render json: {
        my_ads: my_ads,
        pending_ads: pending_ads,
        short_list_ads: short_list_ads
      }
    else
      render json: {
        message: 'No ad found !'
      }
    end
  end
end
