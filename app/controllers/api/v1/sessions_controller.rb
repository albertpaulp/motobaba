class Api::V1::SessionsController < Api::V1::BaseController
  skip_before_filter :authenticate_user!

  def create
    user = User.find_by(email: session_params[:email])
    if user && user.confirmed_at.nil?
      render(
         json: { message: 'Email is not verfied, please verify your email !'},
        status: 403
      )
    elsif user && user.valid_password?(session_params[:password])
      render(
         json: SessionSerializer.new(user, root: false).to_json,
        status: 201
      )
    else
      render(
         json: { message: 'Incorrect email/password !'},
        status: 401
      )
    end
  end

  private

  def session_params
    params.require(:user).permit(:email, :password)
  end
end
