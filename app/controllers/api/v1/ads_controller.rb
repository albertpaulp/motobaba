class Api::V1::AdsController < Api::V1::BaseController

  def index
    render status: :ok, json: {
      message: 'Go through the new API changes.'
    }
  end

  def create
    ad = Ad.new(ads_params)
    ad.ad_type = 'sell'
    ad.name = generate_name
    ad.seller_information.email = seller_email
    ad.is_approved = false
    ad.state = 'initiated'
    ad.user_id = current_user.id
    if ad.save
      if params[:photos]
          params[:photos].each { |image|
            ad.photos.create(image: image)
          }
      end
      render json: {
        message: 'Successful, wait until verification.'
      }
    else
      render json: {
        message: 'Failed, check your inputs'
      }
    end
  end

  def show
    ad = Ad.find_by(id: params[:id])
    if ad.present?
      render json: { ad: ad }
    else
      render json: { message: 'No ad found !' }
    end
  end

  def search
    search_ads = Ad.search do
      if params[:brand] && params[:brand] != ''
        with(:brand_id, params[:brand])
      end
      if params[:model] && params[:model] != ''
        with(:vehicle_model_id, params[:model])
      end
      if params[:emirate] && params[:emirate] != ''
        with(:emirate_id, params[:emirate])
      end
      if params[:min_price] && params[:min_price] != '' && params[:max_price] && params[:max_price] != ''
        with(:price).between(params[:min_price]..params[:max_price])
      end
      with(:is_approved, true)
      order_by :created_at, :desc
      paginate page: params[:page], per_page: 10
    end
    ads = search_ads.results
    if ads.present?
      render json: {
        ads: ads, pages: ads.total_pages
      }
    else
      render json: {
      message: 'No ads found !'
    }
    end
  end

  def destroy
    ad = current_user.ads.where(id: params[:ad_id]).first
    if ad.destroy
      render json: {
        message: 'Successfully deleted.'
      }
    else
      render json: {
        message: 'Failed, try again !'
      }
    end
  end

  private

  def seller_email
    email = ads_params[:seller_information_attributes][:email]
    if email.nil? || email == 'Anonymous'
      'anonymous@abc.com'
    else
      email
    end
  end

  def generate_name
    Brand.where(id: ads_params[:vehicle_detail_attributes][:brand_id]).first.try(:name) + ' ' +
    VehicleModel.where(id: ads_params[:vehicle_detail_attributes][:vehicle_model_id]).first.try(:name)
  end

  def ads_params
    params.require(:ad).permit(:type, :category_id, :country_id, :emirate_id, :location,
      :phone, :status, :description, :price, :negotiable,
      :name, :title, :dealer,
    vehicle_detail_attributes: [:id, :brand_id,
      :vehicle_model_id, :variant_id, :manufacturing_date, :mileage, :fuel, :color,
      :insurance_date, :owner_count, :_destroy, :door_no, :cylinder, :capacity,
      :regional_spec, :horse_power, :cruise_control, :navigation, :fourwheel_drive,
      :allwheel_drive, :body_kit, :climate_control, :keyless_entry, :keyless_start,
      :radio, :aux_audio, :offroad_kit, :offroad_tyre, :parking_sensor, :moonroofs,
      :cd_player, :dvd_player, :power_mirror, :power_seats, :power_sunroof,
      :power_windows, :premium_lights, :premium_paint, :premium_soundsystem,
      :premium_wheels, :racing_seats, :roof_rack, :satellite_radio, :spoiler, :sunroof,
      :winch, :dual_exhaust, :power_steering, :n2o, :tiptronic_gears, :brush_guard,
      :heated_seats, :heat, :cooled_seats, :transmission, :abs, :front_airbags,
      :side_airbags, :bluetooth, :alarm_antitheft],
     seller_information_attributes: [:name, :email, :phone_number])
  end
end
