class Api::V1::RegistrationsController < Devise::RegistrationsController

  def create
    build_resource(sign_up_params)
    if resource.save
      if resource.active_for_authentication?
        return render :status => :ok, :json => {:message => "Successfully created your account"}
      else
        expire_session_data_after_sign_in!
        return render :status => :ok, :json => {:message => "Please confirm your account!"}
      end
    else
      clean_up_passwords resource
      return render :status => 403, :json => {:message => resource.errors.full_messages}
    end
  end

  def update
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)
    logger.debug(params[:user])
    params[:user][:email] = @user.email
    if @user.update_with_password(account_update_params)
      if is_navigational_format?
        update_needs_confirmation?(@user, prev_unconfirmed_email)
      end
      sign_in @user
      return render :status => :ok, :json => {:message => "Password Successfully Updated"}
    else
      clean_up_passwords @user
      return render :status => 403, :json => {message: @user.errors.full_messages}
    end
  end

  def expire_session_data_after_sign_in!
    session.keys.grep(/^devise\./).each { |k| session.delete(k) }
  end

  private 

  def account_update_params
    params.require(:user).permit(:first_name, :last_name, :address, :emirate_id, :city,
      :country, :phone, :gender, :email, :password, :password_confirmation,
      :current_password)
  end

  def sign_up_params
    params.require(:user).permit(:first_name, :last_name, :email, :address, :phone, :emirate_id, :city, :password,
                                 :password_confirmation, :newsletter, :mail_com, :location)
  end
end
