class Api::V1::TemplatesController < Api::V1::BaseController 
  skip_before_filter :authenticate_user!, except: [:get_profile_data]


  def get_brand_names
    brands = Brand.all.pluck(:id, :name)
    render status: :ok, json: { brands: brands }
  end

  def get_model_names
    model_names = VehicleModel.where(brand_id: params[:id]).pluck(:id, :name)
    if model_names.blank?
      render status: :ok, json: { model_names: 'No models available for this brand.' }
    else
      render status: :ok, json: { model_names: model_names }
    end
  end

  def get_emirate_names
    emirates = Emirate.all.pluck(:id, :name)
    render status: :ok, json: { emirates: emirates }
  end

  def get_profile_data
    user = User.find(current_user.id)
    unless user.nil?
      render(
         json: UserSerializer.new(user).to_json,
        status: 201
      )
    else
      render(
         json: { message: 'Incorrect email !'},
        status: 401
      )
    end
  end
end
