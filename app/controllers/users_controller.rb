class UsersController < ApplicationController
	before_action :authenticate_user!

  def dashboard
    @my_ads = current_user.ads
    ad_counts
  end

  def update
    @user = current_user
  	 if @user.update(user_params)
  	 	flash[:notice] = 'Sucessfully updated details !'
  	 else
  	 	flash[:alert] = @user.errors.full_messages.to_sentence
  	 end
    redirect_to users_dashboard_path
  end

  def myads
    @my_ads = current_user.ads
    ad_counts
  end

  def pending_approval_ads
    @my_ads = current_user.ads.unapproved
    ad_counts
  end

  def saved_ads
    short_list_ids = Shortlist.where(user_id:current_user.id).pluck(:ad_id)
    @my_ads = Ad.where(id: short_list_ids)
    ad_counts
  end

  def remove_saved_ad
    ad = Shortlist.find_by_ad_id(params[:id])
    ad.destroy
    render :json => { }
  end

  def destroy
    user = User.find(params[:id])
    user.destroy
    flash[:notice] = 'Account is removed'
    redirect_to root_path
  end

private

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :address, :phone, :emirate_id, :city, :password,
                                 :password_confirmation, :newsletter, :mail_com, :location)
  end

  def ad_counts
    @my_ads_count = current_user.ads.count
    @ad_pending_count = current_user.ads.unapproved.count
    @short_list_count = Shortlist.where(user_id:current_user.id).count
  end
end
