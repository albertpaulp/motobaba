class HomeController < ApplicationController

  def index
    @emirates = Emirate.all
    @models = Brand.first.vehicle_models
  end

  def about_us
  end

  def privacy_policy
  end


  def tos
  end

  def contact
  end

  def faq
  end

  def get_models_from_brand
    models = VehicleModel.where(brand_id: params[:brand])
    render json: models.as_json(only: [:id, :name])
  end

  def enquiry_mail
    UserMailer.enquiry_mail(params[:first_name],params[:last_name],params[:company_name],params[:email],params[:message]).deliver_now
    redirect_to contact_us_path, :notice => 'Successfully sent your message'
  end

end
