module FormatHelper
  def date(date)
    date ? date.strftime("%d:%m:%Y") : 'N/A'
  end

  def data(data)
    data ? data : 'N/A'
  end

  def boolean(data)
    if data
      'Yes'
    else
      'N/A'
    end
  end 
end