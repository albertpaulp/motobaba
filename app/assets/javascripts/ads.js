$(document).on('turbolinks:load', function() {
  $('#insurance_date').datetimepicker({
    format:'d-m-Y',
    timepicker: false
  })

  $("#input-upload-img1").fileinput();
  $("#input-upload-img2").fileinput();
  $("#input-upload-img3").fileinput();
  $("#input-upload-img4").fileinput();
  $("#input-upload-img5").fileinput();
  $("#input-upload-img6").fileinput();

  $('.bxslider').bxSlider({
    pagerCustom: '#bx-pager'
  });

  init_compare();

  $("#submit_form").click(function(){
    if (!$("#terms").is(':checked')){
      alert("Accept our terms and policies to post ad.");
      return false;
    }
  });

  $("#ad_vehicle_detail_attributes_brand_id").change(function(){
    var brand = $("#ad_vehicle_detail_attributes_brand_id").val();
    $.ajax({
      url: "/ads/get_model/",
      data: { id: brand },
      success:function(data){
        $("#ad_vehicle_detail_attributes_vehicle_model_id").empty()
        $.each(data.models, function(index, car){
          $("#ad_vehicle_detail_attributes_vehicle_model_id").append("<option value="+car[0]+">"+car[1]+"</option>")
        })
        $("#ad_vehicle_detail_attributes_variant_id").empty();
        $.each(data.initial_variant, function(index, variant){
          $("#ad_vehicle_detail_attributes_variant_id").append("<option value="+variant[0]+">"+variant[1]+"</option>")
        })
      }
    })
  });

  var user_name = $('#ad_seller_information_attributes_name').val();
  $('#category').change(function(){
    $('#brand').find('option').remove().end().append('<option value="">Select a brand</option>');
    $.ajax({
    type: "get",
    url: '/brands/get_brands_from_category',
    data: { category_id: $('#category').val() },
    success:function(items){
      $.each(items, function (i, item) {
          $('#brand').append($('<option>', {
              value: item.id,
              text : item.name
          }));
      });
    }
    })
  });
  $('#brand').change(function(){
    $('#model').find('option').remove().end().append('<option value="">Select a model</option>');
    $.ajax({
    type: "get",
    url: '/vehicle_models/get_vehicle_models_from_brand',
    data: { brand_id: $('#brand').val() },
    success:function(items){
      $.each(items, function (i, item) {
          $('#model').append($('<option>', {
              value: item.id,
              text : item.name
          }));
      });
    }
    })
  });

  $(".options-notice-show").click(function(){
    $(".options-notice-show").hide();
    $('.options-overlay').slideDown();
    $('.options-notice-hide').show();
  });

  $(".options-notice-hide").click(function(){
    $(".options-notice-show").show();
    $('.options-overlay').slideUp();
    $('.options-notice-hide').hide();
  });


  $(".contact-options-show").click(function(){
    $(".contact-options-show").hide();
    $('.contact-options-overlay').slideDown();
    $('.contact-options-hide').show();
  });

  $(".contact-options-hide").click(function(){
    $(".contact-options-show").show();
    $('.contact-options-overlay').slideUp();
    $('.contact-options-hide').hide();
  });

  $('#hide_name').click(function(){
    if ($('#hide_name').is(":checked")){
      $('#ad_seller_information_attributes_name').val('Anonymous');
    }
    else{
      $('#ad_seller_information_attributes_name').val(user_name);
    }
  });

  $('#hide_phone').click(function(){
    if ($('#hide_phone').is(":checked")){
      $('#ad_seller_information_attributes_phone_number').val('Anonymous');
    }
    else{
      $('#ad_seller_information_attributes_phone_number').val('');
    }
    $('#ad_seller_information_attributes_phone_number').prop('readonly', function(i, v) { return !v; });
  });

  $('#hide_email').click(function(){
    if ($('#hide_email').is(":checked")){
      $('#ad_seller_information_attributes_email').val('Anonymous');
    }
    else{
      $('#ad_seller_information_attributes_email').val('');
    }
    $('#ad_seller_information_attributes_email').prop('readonly', function(i, v) { return !v; });
  });

  $(".save_ad").click(function(e){
    var id = $(this).attr('data-id')
    var user_id = $(this).attr('data-userid')
    if(!$(this).attr('data-userid')){
      alert("Please login to save this ad.");
      return false
    }else{
       e.preventDefault();
      $.ajax({
      type: "POST",
      url: '/ads/save_ad',
      data: {
              id: id,
              user_id: user_id
            },
      dataType: "script",
      error: function(httpObj, textStatus){
        if(httpObj.status == 401){
          alert(httpObj.responseText);
          location.reload(true);
        }
      }
    })
    }
  });

  $(".compare_btn").click(function(){
    $('.compare_box').show();
    if ($('.compare_box').find('.compare_ol').children().length == 3){
      alert('You can only compare 3 items at a time !');
    }
    else {
      $('.compare_ol').append('<div id=' +
                              $(this).prop('id')+ '>' + $('#name_' + $(this).prop('id')).children().text() +
                              '<button type="button" class="close" id="remove_item">&times;</button>' +
                              '</div>')
      $(this).text('Added').attr('disabled', true);
      if (Cookies.getJSON('ads_to_compare') == undefined){
        Cookies.set('ads_to_compare', [{ id: $(this).prop('id'), name: $('#name_' + $(this).prop('id')).children().text() }] )
      }
      else {
        existing_ads = Cookies.getJSON('ads_to_compare');
        existing_ads.push({id: $(this).prop('id'), name: $('#name_' + $(this).prop('id')).children().text() });
        Cookies.set('ads_to_compare', existing_ads);
      }
      $('.compare_btn').each(function() {
        if ($(this).text() != 'Added'){
          $(this).text('Add');
        }
      });
    }
    return false;
  });

  $(document).on('click', '#remove_item', function(){
    $('.compare_btn#' + $(this).parent().prop('id') ).text('Add').attr('disabled', false)
    cookie_temp = Cookies.getJSON('ads_to_compare');
    var element_being_removed = $(this).parent().prop('id');
    $(cookie_temp).each(function(i, val){
      if (element_being_removed == val.id){
        cookie_temp.splice(i, 1);
      }
    })  
    Cookies.set('ads_to_compare', cookie_temp);
    $(this).parent().remove();
    if ($('.compare_box').find('.compare_ol').children().length == 0){
      $('.compare_box').hide();
      $('.compare_btn').each(function() {
        $(this).text('Compare').attr('disabled', false);
      });
    }
  });

  $(".share-btn").click(function(e){
    e.preventDefault();
    $(".share-div").toggle();
  });

  function init_compare(){
    if (Cookies.getJSON('ads_to_compare') == undefined || Cookies.getJSON('ads_to_compare') == ''){
      return false;
    }
    else{
      var ad_ids = new Array();
      $('.compare_box').show();
      $.each(Cookies.getJSON('ads_to_compare'), function(i, val){
      $('.compare_ol').append('<div id=' + val.id + '>' + val.name +
        '<button type="button" class="close" id="remove_item">&times;</button>' +
        '</div>');
      ad_ids.push(val.id);
      });
      $('.compare_btn').each(function() {
        if(jQuery.inArray($(this).prop('id'), ad_ids) !== -1){
          $(this).text('Added').attr('disabled', true);
        }
        else {
          $(this).text('Add');
        }
      });
    }
  }
});
