$(document).on('turbolinks:load', function() {
  $('#brand').change(function(){
    $('#vehicle_model').find('option').remove().end();
    $.ajax({
      type: 'get',
      url: '/home/get_models_from_brand',
      data: { brand: $('#brand').val() },
      success: function(data){
        $.each(data, function(i, item){
          $('#vehicle_model').append($('<option>', {
              value: item.id,
              text : item.name
          }));
        })
      }
    })
  });
});
