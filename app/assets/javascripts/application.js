// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery.min
//= require jquery_ujs
//= require bootstrap.min
//= require fileinput.min
//= require owl.carousel.min
//= require jquery.matchHeight-min
//= require hideMaxListItem
//= require jquery.fs.scroller
//= require jquery.fs.selecter
//= require jquery.bxslider.min
//= require turbolinks
//= require bootstrap-typeahead
//= require pace.min
//= require ads
//= require users
//= require home
//= require jquery.datetimepicker
//= require js.cookie

$(document).on('turbolinks:load', function() {
	paceOptions = {
      elements: true
    };

  $(".item-carousel").owlCarousel({
      autoPlay: 3000, //Set AutoPlay to 3 seconds
      items : 4,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3]
  });
});

