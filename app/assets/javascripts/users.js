$(document).on('turbolinks:load', function() {
	$(".delete-ad-btn").click(function(e){
		if (confirm("Are you sure?") == true) {
	    e.preventDefault();
	    $this = $(this)
	    id = jQuery(this).attr('id')
	    jQuery.ajax({
	      type: "DELETE",
	      url: "/ads/"+id,
	      dataType: "script",
	      error: function(httpObj, textStatus){
	        if(httpObj.status == 401){
	          alert(httpObj.responseText);
	          location.reload(true);
	        }
	      }
	    }).success(function(json){
	    	$this.closest('tr').remove();
	    });
	  }
	  return false;
	});

	$(".remove-saved_ad-btn").click(function(e){
		if (confirm("Are you sure?") == true) {
	    e.preventDefault();
	    $this = $(this)
	    id = jQuery(this).attr('id')
	    jQuery.ajax({
	      type: "PUT",
	      url: "/users/remove_saved_ad",
	      data: {id: id},
	      dataType: "script",
	      error: function(httpObj, textStatus){
	        if(httpObj.status == 401){
	          alert(httpObj.responseText);
	          location.reload(true);
	        }
	      }
	    }).success(function(json){
	    	$this.closest('tr').remove();
	    	alert("Ad is sucessfully removed from your list")
	    });
	  }
	  return false;
	});
});
